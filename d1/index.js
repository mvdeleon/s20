// console.log("hello world");

// Repitition Control Structure (Loops)

function greeting(){
	console.log("Hi, Batch 211!");
};

greeting();
greeting();
greeting();
greeting();
greeting();
greeting();
greeting();
greeting();
greeting();
greeting();

let countNum = 10;

while(countNum !==0){
	console.log("This is printed inside the loop: " + countNum);
	greeting()
	countNum--;
}

// While Loop

let count = 5;

while(count !==0){
	console.log("While: " + count);
	count --;
}

// let num = 20;

// while(num !==0){
// 	console.log("While: num " + num)
// 	num --;
// 	}


// let digit = 5;

// while(digit !== 20){
// 	console.log("While: digit " + digit)
// 	digit ++;
// }

// let num1 = 1;
// while(num1 === 2){
// 	console.log("While: num1 " + num1);
// 	num--
// }

// Do While

// let number = Number(prompt("Give me a number:"));
// do {
// 	console.log("Do while: " + number)
// 	number += 1;
// }while(number<10)

// For Loop

// for (let count = 0; count <=20; count++){
// 	console.log("First Loop: " + count);
// }

// // Activity
// for (let count = 0; count<=20; count++){
// 	 if(count % 2 == 0){
// 	 	console.log(count);
// 	 }
// }

let myString = "Basty Church";
console.log(myString.length);

console.log(myString[2]);
console.log(myString[0]);
console.log(myString[8]);
console.log(myString[11]);

for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
};

let myName = "NehEmIAh";
let myNewName = "";

for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "u" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o" 
		){
		console.log(3);
	}else{
		console.log(myName[i]);
		myNewName += myName[i];
	}
}

console.log(myNewName);

// Continue and Break Statements

for(let count = 0; count <=20; count++){
	if(count % 2 === 0){
		console.log("Even Number")
		continue;
		// console.log("Hello")
	}
	console.log("Continue and Break" + count);

	if(count>10){
		break;
	}
}

// let name = "alexandro";
 
// for(let i = 0; i<name.length; i++){
// 	console.log(name[i]);


// if(name[i].toLowerCase() ==== "a"){
// 	console.log("Continue to the next iteration");
// 	continue;
// }

// if(name[i] == "d"){
// 	break;
// }
// }
